// === Import Sass ===
import './scss/main.scss';

// === Import JS Plugins ===
import Vue from 'vue';
import vOutsideEvents from 'vue-outside-events';
import VueTextareaAutosize from 'vue-textarea-autosize';
import VueYouTubeEmbed from 'vue-youtube-embed';
import vueVimeoPlayer from 'vue-vimeo-player';
import VueDragDrop from 'vue-drag-drop';
import { focus } from 'vue-focus';

// === Config Vue ===
Vue.use(vOutsideEvents);
Vue.use(VueTextareaAutosize);
Vue.use(VueYouTubeEmbed);
Vue.use(vueVimeoPlayer);
Vue.use(VueDragDrop);

// === Import components ===
import PostEditor from './components/NewPost.vue';

export default PostEditor;