import Velocity from 'velocity-animate';

export const PopoverTransition = {
    methods: {
        popoverEnter(el, done) {
            // Velocity(el, 'stop');
            Velocity(el,
                {
                    opacity: 1,
                    top: +el.dataset.position + 40,
                    scale: 1
                },
                {
                    delay: 200,
                    duration: 500,
                    easing:this.$root.easing,
                    complete: done
                }
            );
        },
        popoverLeave(el, done) {
            Velocity(el, 'stop');
            Velocity(el,
                {
                    opacity: 0,
                    top: +el.dataset.position,
                    scale: 0.8,
                },
                {
                    duration: 500,
                    easing:this.$root.easing,
                    complete: done
                }
            );
        },
    }
};

export const PopoverTransitionUp = {
    methods: {
        popoverUpEnter(el, done) {
            // Velocity(el, 'stop');
            Velocity(el,
                {
                    opacity: 1,
                    top: +el.dataset.position - 40,
                    scale: 1
                },
                {
                    delay: 200,
                    duration: 500,
                    easing:this.$root.easing,
                    complete: done
                }
            );
        },
        popoverUpLeave(el, done) {
            Velocity(el, 'stop');
            Velocity(el,
                {
                    opacity: 0,
                    top: +el.dataset.position,
                    scale: 0.8,
                },
                {
                    duration: 500,
                    easing:this.$root.easing,
                    complete: done
                }
            );
        },
    }
};

export const PopoverTransitionDown = {
    methods: {
        popoverDownEnter(el, done) {
            Velocity(el, 'stop');
            Velocity(el,
                {
                    opacity: 1,
                    bottom: +el.dataset.position - 40,
                    scale: 1
                },
                {
                    delay: 200,
                    duration: 500,
                    easing:this.$root.easing,
                    complete: done
                }
            );
        },
        popoverDownLeave(el, done) {
            Velocity(el, 'stop');
            Velocity(el,
                {
                    opacity: 0,
                    bottom: +el.dataset.position,
                    scale: 0.8,
                },
                {
                    duration: 500,
                    easing:this.$root.easing,
                    complete: done
                }
            );
        },
    }
};

export const DropdownTransition = {
    methods: {
        dropDownEnter(el, done) {
            let parent = el.parentElement,
                caret = parent.getElementsByClassName('caret-icon')[0];
            parent.classList.add('active');
            if (caret) {
                Velocity(caret, 'stop');
                Velocity(caret,
                    {
                        rotateZ: -180
                    },
                    {
                        duration: 500,
                        easing:this.$root.easing,
                    }
                );
            }
            Velocity(el, 'stop');
            Velocity(el,
                {
                    opacity: 1,
                    scaleY: 1
                },
                {
                    duration: 500,
                    easing:this.$root.easing,
                    complete: done
                }
            );
        },
        dropDownLeave(el, done) {
            let parent = el.parentElement,
                caret = parent.getElementsByClassName('caret-icon')[0];
            parent.classList.remove('active');
            if (caret) {
                Velocity(caret, 'stop');
                Velocity(caret,
                    {
                        rotateZ: 0
                    },
                    {
                        duration: 500,
                        easing:this.$root.easing,
                    }
                );
            }
            Velocity(el, 'stop');
            Velocity(el,
                {
                    opacity: 0,
                    scaleY: 0
                },
                {
                    duration: 500,
                    easing:this.$root.easing,
                    complete: done
                }
            );
        },
    }
};

export const NewPostPageTransitions = {
    methods: {
        contentEnter(el, done) {
            Velocity(el, 'stop');
            Velocity(el,
                {
                    opacity: 1,
                },
                {
                    duration: 300,
                    easing: this.$root.easeInOutQuint,
                    complete: done
                }
            );
        },
        contentLeave(el, done) {
            Velocity(el, 'stop');
            Velocity(el,
                {
                    opacity: 0,
                },
                {
                    duration: 300,
                    easing: this.$root.easeInOutQuint,
                    complete: done
                }
            );
        },
        tagLeave(el, done) {
            Velocity(el, 'stop');
            Velocity(el,
                {
                    opacity: 0,
                    scale: 0,
                },
                {
                    duration: 300,
                    easing: this.$root.easeInOutQuint,
                    complete: () => {
                        done();
                        this.tags.list = this.tags.list.filter((item) => {return item.show})
                    }
                }
            );
        }
    }
};

export const NewPostContentTransitions = {
    methods: {
        postContentLeave(el, done) {
            Velocity(el, 'stop');
            Velocity(el,
                {
                    opacity: 0,
                },
                {
                    duration: 300,
                    easing: this.$root.easeInOutQuint,
                }
            );
            Velocity(el,
                {
                    height: 0,
                    margin: 0,
                    padding: 0
                },
                {
                    duration: 300,
                    easing: this.$root.easeInOutQuint,
                    complete: () => {
                        done();
                        if (el.classList.contains('button-wrapper')) {
                            this.checkIsActiveButtons();
                        } else {
                            this.$parent.removeBlock(this.itemToDelete);
                            this.$parent.diactivateAllTools();
                            this.itemToDelete = null;
                        }
                    }
                }
            );
        }
    }
};

export const ContentToolsTransitions = {
    methods: {
        deletePopoverEnter: function (el, done) {
            let move = 0;
            if ( window.window.innerWidth >= 960) {
                el.className = el.className.replace(/\bn1f\b/g, "n1i");


                let startPosition = -parseInt(window.getComputedStyle(el).width)+20;
                el.dataset.position = startPosition;
                el.style.left = startPosition;
                el.style.top = 'auto';
                move = -40;

                Velocity(el, 'stop');
                Velocity(el,
                    {
                        opacity: 1,
                        left: +el.dataset.position + move,
                        scale: 1
                    },
                    {
                        duration: 500,
                        easing:this.$root.easing,
                        complete: done
                    }
                );
            } else {
                el.className = el.className.replace(/\bn1i\b/g, "n1f");
                el.dataset.position = 40;
                el.style.top = 40;
                el.style.left = 'auto';
                move = 40;

                Velocity(el, 'stop');
                Velocity(el,
                    {
                        opacity: 1,
                        top: +el.dataset.position + move,
                        scale: 1
                    },
                    {
                        duration: 500,
                        easing:this.$root.easing,
                        complete: done
                    }
                );
            }
        },
        deletePopoverLeave: function (el, done) {
            if (window.window.innerWidth >= 960) {
                Velocity(el, 'stop');
                Velocity(el,
                    {
                        opacity: 0,
                        left: +el.dataset.position,
                        scale: 0.8,
                    },
                    {
                        duration: 500,
                        easing:this.$root.easing,
                        complete: done
                    }
                );
            } else {
                Velocity(el, 'stop');
                Velocity(el,
                    {
                        opacity: 0,
                        top: +el.dataset.position,
                        scale: 0.8,
                    },
                    {
                        duration: 500,
                        easing:this.$root.easing,
                        complete: done
                    }
                );
            }

        },
    }
};



