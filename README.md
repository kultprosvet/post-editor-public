
Post Editor
============

Version: 1.0.0

Introduction
------------

This is a simple post editor based on Vue 2.0 with clear and responsive design.

The following tools are presented:
- Bold text
- Italic text
- Links
- Heading 1
- Heading 2
- Heading 3
- Bulleted list
- Numbered list
- Blockquote
- Image
- Video (YouTube or Vimeo links are supported)
- Text divider

Also you can edit code of the post by using "Code" tool. 


Installation
------------

##### Add repository to `package.json`

```json
"repository": {
    "type": "git",
    "url": "https://bitbucket.org/kultprosvet/post-editor-public.git"
  }
```

##### Install package 
```
npm install post-editor --save
```

##### Import **PostEditor** component from **./post-editor**
```
import PostEditor from './post-editor';
```

Usage
------------

##### Use **<post-editor>** tag inside your Vue application
```
<post-editor></post-editor
```

##### Use props **:post-to-edit** to pass post object you want to edit
```
<post-editor :post-to-edit="postObject"></post-editor
```

##### Listen to the **post-created** event in the $root to get post
```javascript
this.$root.$on('post-created', (postObject) => doSomething());
```